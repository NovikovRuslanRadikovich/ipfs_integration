package ru.kpfu.itis.services;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class IpfsService {

    public IPFS getIpfs() {
        return ipfs;
    }

    IPFS ipfs = null;

    @PostConstruct
    public void main() throws IOException {
        // Can't do this in the constructor because the RestTemplate injection
        // happens afterwards.
        // @see: https://github.com/ipfs/java-ipfs-api
        IPFS ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
        ipfs.refs.local();
    }


    public byte[] get(String hashStr) throws IOException {
        Multihash filePointer = Multihash.fromBase58(hashStr);//"QmPZ9gcCEpqKTo6aq61g2nXGUhM4iCL3ewB6LDXZCtioEB"
        byte[] fileContents = ipfs.cat(filePointer);
        ipfs.get()
//        logger.info("IpfsService get() for " + hashStr);
        return fileContents;
    }

}
