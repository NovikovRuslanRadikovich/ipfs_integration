package ru.kpfu.itis.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.services.IpfsService;

import java.io.IOException;

@RestController
@RequestMapping(value = "v1/ipfs")
public class IpfsController {

    @Autowired
    IpfsService ipfsService;

    @RequestMapping(method = RequestMethod.GET, value = "/{hashID}")
    public @ResponseBody
    ResponseEntity<?> getOne(@PathVariable("hashID") String hashID) throws IOException {
        byte[] fileContents = ipfsService.get(hashID);
        return new JsonObject(fileContents);
    }
}
