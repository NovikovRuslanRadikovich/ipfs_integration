package ru.kpfu.itis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpfsIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(IpfsIntegrationApplication.class, args);
    }

}
